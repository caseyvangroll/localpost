import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Container, Row, Input } from 'reactstrap';
import { isWebUri } from 'valid-url';
import './index.css';

console.log(isWebUri('balh'));
class App extends Component {
  state = {
    url: `http://localhost:8080`
  }

  setURL = event => this.setState({ url: event.target.value })
  componentDidUpdate = () => console.log(this.state.url)

  render() {
    return (
      <Container>
        <Row>
          <Input
            valid={isWebUri(this.state.url) !== undefined}
            id='localpost_url'
            onChange={this.setURL}
            defaultValue={this.state.url}/>
        </Row>
      </Container>
    );
  }
};

ReactDOM.render(<App />, document.getElementById('root'));
