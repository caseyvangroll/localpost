
var version = "1.2";
const url = `https://localhost:8080`;
const debug = false;

let devtoolsConnection;

chrome.runtime.onConnect.addListener(port => {
    port.onMessage.addListener((message, sender, sendResponse) => {
      if (message.name == "init")
        devtoolsConnection = port;
  });
});

// Receive message from content script and relay to the devTools page
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (sender.tab) {
      var tabId = sender.tab.id;
      if (tabId === devtoolsConnection) {
        devtoolsConnection.postMessage(request);
      } else {
        console.log("Tab not found in connection list.");
      }
    } else {
      console.log("sender.tab not defined.");
    }
    return true;
});

chrome.browserAction.onClicked.addListener(tab => {
  const notify = message => {
    chrome.tabs.sendMessage(tab.id, message);
    devtoolsConnection.postMessage({responseBody: JSON.stringify(message)});
  }
  notify('clicked');

  const target = { tabId: tab.id };
  chrome.debugger.attach(target, version, () => {
    notify('attached');
    chrome.debugger.sendCommand(target, "Network.enable");

    chrome.debugger.onEvent.addListener((debuggeeId, message, params) => {
      notify(message);
      if (message === 'Network.responseReceived') {
        notify('received params')
        chrome.debugger.sendCommand(
          target,
          "Network.getResponseBody",
          { requestId: params.requestId },
          body => {
            if (body) {
              notify(body);
              // var xhr = new XMLHttpRequest();
              // xhr.open("POST", yourUrl, true);
              // xhr.setRequestHeader('Content-Type', 'application/json');
              // xhr.send(JSON.stringify({
              //   value: value
              // }));
            }
            else notify('Unable to retrieve contract');
          }
        );
      }
    });

  });
});
